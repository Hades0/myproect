from PyQt5.QtWidgets import QMainWindow, QTableWidgetItem, QMessageBox
from PyQt5.QtWidgets import QFileDialog
from ui_py import ui_main_window1
from windows import dialog_add_edit
import csv
import json

class MainWindow(QMainWindow, ui_main_window1.Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.pushButton_add.clicked.connect(self.btn_add_click)
        self.pushButton_4JSON.clicked.connect(self.btn_add_click_json)
        self.pushButton_del.clicked.connect(self.btn_delete_click)
        self.pushButton_edit.clicked.connect(self.btn_edit_click)
        self.pushButton_1CSV.clicked.connect(self.btn_from_file_click)
        self.pushButton_2CSV.clicked.connect(self.btn_to_file_click)
        self.pushButton_1JSON.clicked.connect(self.btn_from_file_click_json)
        self.pushButton_2JSON.clicked.connect(self.btn_to_file_click_json)
       # self.pushButton_3JSON.clicked.connect(self.btn_showcsv_click)

    def btn_add_click(self):
        dlg = dialog_add_edit.DialogAddEditStudent(self, mode='add', app_name=self.windowTitle())
        dlg.show()
        res = dlg.exec_()
        if res:
            data = dlg.get_data()
            row = self.tableWidget.rowCount()
            self.tableWidget.setRowCount(row + 1)
            self.tableWidget.setItem(row, 0, QTableWidgetItem(data['lastName']))
            self.tableWidget.setItem(row, 1, QTableWidgetItem(data['firstName']))
            self.tableWidget.setItem(row, 2, QTableWidgetItem(data['middleName']))
            self.tableWidget.setItem(row, 3, QTableWidgetItem(data['group']))

    def btn_add_click_json(self):
        dlg = dialog_add_edit.DialogAddEditStudent(self, mode='add', app_name=self.windowTitle())
        dlg.show()
        res = dlg.exec_()
        if res:
            data = dlg.get_data()
            row = self.tableWidget_2.rowCount()
            self.tableWidget_2.setRowCount(row + 1)
            self.tableWidget_2.setItem(row, 0, QTableWidgetItem(data['lastName']))
            self.tableWidget_2.setItem(row, 1, QTableWidgetItem(data['firstName']))
            self.tableWidget_2.setItem(row, 2, QTableWidgetItem(data['middleName']))
            self.tableWidget_2.setItem(row, 3, QTableWidgetItem(data['group']))

    def btn_delete_click(self):
        msg = QMessageBox.question(self,
                                   self.windowTitle(),
                                   'Вы действительно хотите удалить запись?',
                                   QMessageBox.Yes | QMessageBox.No)
        if msg == QMessageBox.Yes:
            row = self.tableWidget.currentRow()
            self.tableWidget.removeRow(row)

    def btn_edit_click(self):
        row = self.tableWidget.currentRow()
        if row != -1:
            data = {
                'lastName': self.tableWidget.item(row, 0).text(),
                'firstName': self.tableWidget.item(row, 1).text(),
                'middleName': self.tableWidget.item(row, 2).text(),
                'group': self.tableWidget.item(row, 3).text(),
            }
            dlg = dialog_add_edit.DialogAddEditStudent(self,
                                                       mode='edit',
                                                       app_name=self.windowTitle(),
                                                       data=data)
            dlg.show()
            res = dlg.exec_()
            if res:
                data = dlg.get_data()
                self.tableWidget.item(row, 0).setText(data['lastName'])
                self.tableWidget.item(row, 1).setText(data['firstName'])
                self.tableWidget.item(row, 2).setText(data['middleName'])
                self.tableWidget.item(row, 3).setText(data['group'])
        else:
            QMessageBox.warning(self,
                                self.windowTitle(),
                                'Строка для редактирования не выбрана!')

    def btn_from_file_click(self):
        fd = QFileDialog.getOpenFileName(self,
                                         f'{self.windowTitle()} [открыть]',
                                         '',
                                         'CSV (*.csv);;txt (*.txt);;All (*.*)')
        if fd[0]:
            with open(fd[0], 'r', encoding='utf-8') as f:
                reader = csv.reader(f, delimiter=';')
                elems = list(reader)
            elems.pop(0)
            for data in elems:
                row = self.tableWidget.rowCount()
                self.tableWidget.setRowCount(row + 1)
                self.tableWidget.setItem(row, 0, QTableWidgetItem(data[0]))
                self.tableWidget.setItem(row, 1, QTableWidgetItem(data[1]))
                self.tableWidget.setItem(row, 2, QTableWidgetItem(data[2]))
                self.tableWidget.setItem(row, 3, QTableWidgetItem(data[3]))

    def btn_to_file_click(self):
        fd = QFileDialog.getSaveFileName(self,
                                         f'{self.windowTitle()} [путь для сохранения]',
                                         '',
                                         'CSV (*.csv);;txt (*.txt);;All (*.*)')
        if fd[0]:
            with open(fd[0], 'w', encoding='utf-8', newline='') as f:
                writer = csv.writer(f, delimiter=';')
                writer.writerow([
                    self.tableWidget.horizontalHeaderItem(0).text(),
                    self.tableWidget.horizontalHeaderItem(1).text(),
                    self.tableWidget.horizontalHeaderItem(2).text(),
                    self.tableWidget.horizontalHeaderItem(3).text()
                ])
                for i in range(self.tableWidget.rowCount()):
                    line = []
                    line.append(self.tableWidget.item(i, 0).text())
                    line.append(self.tableWidget.item(i, 1).text())
                    line.append(self.tableWidget.item(i, 2).text())
                    line.append(self.tableWidget.item(i, 3).text())
                    writer.writerow(line)
            self.statusbar.showMessage(f'Файл {fd[0]} успешно записан!')

    def btn_from_file_click_json(self):
        fd = QFileDialog.getOpenFileName(self,
                                         f'{self.windowTitle()} [открыть]',
                                         '',
                                         'JSON (*.json);;txt (*.txt);;All (*.*)')
        if fd[0]:
            with open(fd[0], 'r', encoding='utf-8') as f:
                json_data = f.read
                print(json_data)

                #d = {'repeinik': 26, 'podberezowik': 24, 'Павел_Шуков_talks48': 20, 'stupich': 14, 'kipovets': 14,
                #     'Лайош_Портиш': 13, 'vijigau_mraz': 13, 'наивная баба': 12, 'genek77': 12}
               # print("{:<20} {:<15}".format('Name', 'Value'))
               # for v in d.items():
                #    name, val = v
                #    print("{:<20} {:<15}".format(name, val))

    def btn_to_file_click_json(self):
        fd = QFileDialog.getSaveFileName(self,
                                         f'{self.windowTitle()} [путь для сохранения]',
                                         '',
                                         'JSON (*.json);;txt (*.txt);;All (*.*)')
        if fd[0]:
            with open(fd[0], 'w', encoding='utf-8', newline='') as f:
                json_data = f.write(input())
            print(json_data)
            self.statusbar.showMessage(f'Файл {fd[0]} успешно записан!')

    def closeEvent(self, event):
        close = QMessageBox.question(self,
                                   self.windowTitle(),
                                   'Вы действительно хотите выйти?',
                                   QMessageBox.Yes | QMessageBox.Cancel)

        if close == QMessageBox.Yes:
            event.accept()

        else:
            event.ignore()

  #  def btn_showcsv_click(self):
   #     self.show_window = ViewingCsvWindow()
   #     self.show_window.show()