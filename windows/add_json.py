from PyQt5.QtWidgets import QDialog
from ui_py import ui_dialog_add_json


class DialogAddEditStudent(QDialog, ui_dialog_add_json.Ui_Dialog):
    def __init__(self, parent=None, mode='add', app_name='', data=None):
        super().__init__(parent)
        self.setupUi(self)
        if mode == 'add':
            self.setWindowTitle(f'{app_name} [Редактирование]')
        else:
            self.setWindowTitle(f'{app_name} [Редактирование]')
            self.lineEdit_lastName.setText(data['lastName'])
            self.lineEdit_firstName.setText(data['firstName'])
            self.lineEdit_middleName.setText(data['middleName'])
            self.lineEdit_group.setText(data['group'])

    def get_data(self):
        return {
            'lastName': self.lineEdit_lastName.text(),
            'firstName': self.lineEdit_firstName.text(),
            'middleName': self.lineEdit_middleName.text(),
            'group': self.lineEdit_group.text(),
        }
